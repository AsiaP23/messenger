package pl.sdacademy.chat.service;

public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}
