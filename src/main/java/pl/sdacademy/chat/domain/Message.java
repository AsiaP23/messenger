package pl.sdacademy.chat.domain;

public class Message {

	private Long id;
    private String text;
    private String date;
    private Long idRoom;

    public Message(){}

    public Message(String textMessage, String dateMessage) {
        this.text = textMessage;
        this.date = dateMessage;
    }
}
