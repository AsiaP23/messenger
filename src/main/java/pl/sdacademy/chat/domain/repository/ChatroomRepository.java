package pl.sdacademy.chat.domain.repository;

import java.util.List;

import pl.sdacademy.chat.domain.Chatroom;

public interface ChatroomRepository {
	
	Chatroom findById(Long Id);
	Chatroom findByName(String name);
	List<Chatroom> findByIsActive (boolean isActive);
}
