package pl.sdacademy.chat.domain.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.chat.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
