package pl.sdacademy.chat.domain.repository;

import java.util.List;

import pl.sdacademy.chat.domain.Message;

public interface MessageRepository {

	Message findById(Long id);
	List<Message> findByIdChatroom(Long id);
}
