package pl.sdacademy.chat.domain;

public class Chatroom {
	
	private Integer id;
	private String name;
	private Boolean isActive;
	
	public Chatroom() {
	}
	
	public Chatroom(Boolean isActive){
		this.isActive = isActive;
	}

	public Integer getChatroomId() {
		return id;
	}

	public void setChatroomId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	

}
