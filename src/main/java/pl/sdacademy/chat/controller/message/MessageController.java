package pl.sdacademy.chat.controller.message;

import java.security.Principal;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.sdacademy.chat.domain.repository.MessageRepository;
import pl.sdacademy.chat.domain.repository.UserRepository;

@RestController
@RequestMapping("/messages")
public class MessageController {
	
	private MessageRepository messageRepository;
	private UserRepository userRepository;
	
	@RequestMapping("/message")
    public Principal message(Principal message) {
        return message;
    }

}
