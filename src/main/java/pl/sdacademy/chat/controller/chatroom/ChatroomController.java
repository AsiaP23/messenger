package pl.sdacademy.chat.controller.chatroom;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import pl.sdacademy.chat.domain.Chatroom;

@Controller
public class ChatroomController {

	@RequestMapping(name = "/chatroom", method = RequestMethod.GET)
	public String welcome(Model model) {
		Chatroom first = new Chatroom(true);
		first.setName("nr 1");
		first.setChatroomId(00000000001);
		model.addAttribute("chatroom", first);
		return "chatroom";
	}
}
